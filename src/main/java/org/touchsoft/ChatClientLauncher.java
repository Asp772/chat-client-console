package org.touchsoft;

import org.apache.log4j.Logger;
import org.touchsoft.app.ChatClient;
import org.touchsoft.config.ClientConfig;
import org.touchsoft.exception.ChatClientException;
import org.touchsoft.service.ActionTransformService;
import org.touchsoft.service.ClientActionReceiverService;
import org.touchsoft.service.impl.ActionTransformServiceImpl;
import org.touchsoft.service.impl.ClientActionReceiverServiceImpl;

public class ChatClientLauncher {
    private final static Logger logger = Logger.getLogger(ChatClientLauncher.class.getName());

    public static void main(String[] args) {
        logger.info("trying to start app");
        ClientActionReceiverService clientActionReceiverService = new ClientActionReceiverServiceImpl();
        ActionTransformService actionTransformService = new ActionTransformServiceImpl();

        ChatClient chatClient = new ChatClient(ClientConfig.SERVER_ADDR, clientActionReceiverService
                , actionTransformService);
        try {
            chatClient.launch();
        } catch (ChatClientException e) {
            logger.error(e.getMessage());
            System.err.println(e.getMessage());
        }

    }
}
