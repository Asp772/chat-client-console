package org.touchsoft.manager;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.client.ClientUpgradeRequest;
import org.eclipse.jetty.websocket.client.WebSocketClient;
import org.touchsoft.exception.*;
import org.touchsoft.model.SocketWrapper;
import org.touchsoft.model.action.Action;
import org.touchsoft.service.ActionTransformService;
import org.touchsoft.service.ClientActionReceiverService;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class ClientManager {
    private WebSocketClient webSocketClient;
    private SocketWrapper socket;
    private ClientActionReceiverService clientActionReceiverService;

    public ClientManager(ClientActionReceiverService clientActionReceiverService,
                         ActionTransformService actionTransformService) {
        this.webSocketClient = new WebSocketClient();
        this.socket = new SocketWrapper(actionTransformService);
        this.clientActionReceiverService = clientActionReceiverService;
    }

    public void startClient() throws StartClientException {
        try {
            this.webSocketClient.start();
        } catch (Exception e) {
            throw new StartClientException(e.getMessage());
        }
    }

    public void connectClient(String serverAdddr) throws ConnectClientException, WrongServerParameterException {
        ClientUpgradeRequest request = new ClientUpgradeRequest();
        URI echoUri;

        try {
            echoUri = new URI(serverAdddr);
            Future<Session> future = this.webSocketClient.connect(this.socket, echoUri, request);
            future.get();

        } catch (URISyntaxException e) {
            throw new WrongServerParameterException(e.getMessage());
        } catch (InterruptedException e) {
            throw new ConnectClientException(e.getMessage());
        } catch (ExecutionException e) {
            throw new ConnectClientException(e.getMessage());
        } catch (IOException e) {
            throw new ConnectClientException(e.getMessage());
        }
    }

    public void interact() throws StopClientException {
        Action action;
        action = this.clientActionReceiverService.receiveAction();
//        System.out.println(action);

        while (action != null) {
            try {
                this.socket.sendAction(action);
            } catch (SendActionException e) {
                e.printStackTrace();
            }
            action = this.clientActionReceiverService.receiveAction();
        }

        stopClient();
    }

    private void stopClient() throws StopClientException {
        try {
            this.socket.awaitClose(5, TimeUnit.SECONDS);
        } catch (SocketCloseException e) {
            throw new StopClientException(e.getMessage());
        }
        try {
            this.webSocketClient.stop();
        } catch (Exception e) {
            throw new StopClientException(e.getMessage());
        }
    }
}
