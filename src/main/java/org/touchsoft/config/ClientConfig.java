package org.touchsoft.config;

import java.util.ResourceBundle;

public class ClientConfig {
    private static final String PROPERTIES_FILE = "application";
    private static final String SERVER_ADDR_NAME = "SERVER-ADDRESS";

    public static String SERVER_ADDR;

    static {
        ResourceBundle resourceBundle = ResourceBundle.getBundle(PROPERTIES_FILE);
        SERVER_ADDR = resourceBundle.getString(SERVER_ADDR_NAME);
    }
}