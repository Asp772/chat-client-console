package org.touchsoft.model;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.*;
import org.touchsoft.exception.ActionToStringTransformException;
import org.touchsoft.exception.SendActionException;
import org.touchsoft.exception.SocketCloseException;
import org.touchsoft.model.action.Action;
import org.touchsoft.service.ActionTransformService;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@WebSocket
public class SocketWrapper {
    private final CountDownLatch closeLatch;
    private ActionTransformService actionTransformService;

    @SuppressWarnings("unused")
    private Session session;

    public SocketWrapper(ActionTransformService actionTransformService) {
        this.closeLatch = new CountDownLatch(1);
        this.actionTransformService = actionTransformService;
    }

    public boolean awaitClose(int duration, TimeUnit unit) throws SocketCloseException {
        try {
            return this.closeLatch.await(duration, unit);
        } catch (InterruptedException e) {
            throw new SocketCloseException(e.getMessage());
        }
    }
    @OnWebSocketError
    public void onError(Throwable e){
        e.printStackTrace();
    }

    @OnWebSocketClose
    public void onClose(int statusCode, String reason) {
        System.out.printf("Connection closed: %d - %s%n", statusCode, reason);
        this.session = null;
        this.closeLatch.countDown(); // trigger latch
    }

    @OnWebSocketConnect
    public void onConnect(Session session) {
        System.out.printf("Got connect: %s%n", session);
        this.session = session;
    }

    @OnWebSocketMessage
    public void onMessage(String msg) {
        this.actionTransformService.displayAction(msg);

    }

    public void sendAction(Action action) throws SendActionException {
        String str = null;
        try {
            str = this.actionTransformService.toString(action);
            this.session.getRemote().sendStringByFuture(str);
        } catch (ActionToStringTransformException e) {
           throw new SendActionException(e.getMessage());
        }

    }
}
