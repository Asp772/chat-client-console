package org.touchsoft.exception;

public class WrongServerParameterException extends Exception {
    public WrongServerParameterException(String message) {
        super(message);
    }
}
