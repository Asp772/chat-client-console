package org.touchsoft.exception;

import java.io.IOException;

public class ConnectClientException extends IOException {
    public ConnectClientException(String s) {
        super(s);
    }
}
