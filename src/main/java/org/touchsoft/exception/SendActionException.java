package org.touchsoft.exception;

public class SendActionException extends Throwable {
    public SendActionException(String s) {
        super(s);
    }
}
