package org.touchsoft.exception;

public class StartClientException extends Exception {
    public StartClientException(String s) {
        super(s);
    }
}
