package org.touchsoft.exception;

import java.io.IOException;

public class ChatClientException extends IOException {
    public ChatClientException(String message) {
        super(message);
    }
}
