package org.touchsoft.exception;

public class ActionToStringTransformException extends Exception {
    public ActionToStringTransformException(String s) {
        super(s);
    }
}
