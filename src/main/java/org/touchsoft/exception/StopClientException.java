package org.touchsoft.exception;

import java.io.IOException;

public class StopClientException extends IOException {
    public StopClientException(String s) {
        super(s);
    }
}
