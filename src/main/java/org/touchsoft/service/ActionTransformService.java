package org.touchsoft.service;

import org.touchsoft.exception.ActionToStringTransformException;
import org.touchsoft.model.action.Action;

public interface ActionTransformService {
    String toString(Action action) throws ActionToStringTransformException;

    void displayAction(String action);
}
