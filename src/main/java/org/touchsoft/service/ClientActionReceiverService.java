package org.touchsoft.service;

import org.touchsoft.model.action.Action;

public interface ClientActionReceiverService {
    Action receiveAction();
}
