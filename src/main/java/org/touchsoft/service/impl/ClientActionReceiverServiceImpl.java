package org.touchsoft.service.impl;

import org.touchsoft.exception.*;
import org.touchsoft.model.action.Action;
import org.touchsoft.model.action.ActionType;
import org.touchsoft.model.action.payload.impl.MessagePayload;
import org.touchsoft.model.action.payload.impl.Role;
import org.touchsoft.model.action.payload.impl.VisitorData;
import org.touchsoft.service.ClientActionReceiverService;
import org.touchsoft.util.SafePrint;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ClientActionReceiverServiceImpl implements ClientActionReceiverService {
    private boolean isRegistered;

    public ClientActionReceiverServiceImpl() {
        this.isRegistered = false;
    }

    @Override
    public Action receiveAction() {
        if (!isRegistered) {
            Action action = this.register();

            if (action != null) {
                this.isRegistered = true;
            }

            return action;
        } else {
            return this.communicate();
        }
    }

    private String getClientCommand() throws ClientActionUnhandledException {
        SafePrint.safePrintln("Enter command:");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            return br.readLine();
        } catch (IOException e) {
            throw new ClientActionUnhandledException("Wrong text entered");
        }
    }

    public Action communicate() {
        Action clientAction = null;

        try {
            String enteredString = getClientCommand();
            String retval[] = enteredString.split(" ", 3);

            switch (retval[0]) {

                case "/leave": {
                    clientAction = new Action(ActionType.LEAVE, new MessagePayload("User has left"));
                }
                break;

                default: {
                    clientAction = new Action(ActionType.SEND_MESSAGE, new MessagePayload(enteredString));
                }
                break;
            }
        } catch (ClientActionUnhandledException e) {
            System.err.println(e.getMessage());
        }

        return clientAction;
    }


    private Action register() {
        Action registerAction = null;
        boolean continueActivity = true;

        while (continueActivity) {
            try {
                String enteredString = getClientCommand();
                String retval[] = enteredString.split(" ", 3);

                switch (retval[0]) {

                    case "/register": {
                        Role role = Role.fromString(retval[1]);
                        if (role == null) throw new ClientActionUnhandledException("unknown command was entered");
                        VisitorData visitorData = new VisitorData(retval[2], role);

                        ActionType actionType = ActionType.REGISTER;
                        registerAction = new Action(actionType, visitorData);

                        continueActivity = false;
                    }
                    break;

                    case "/leave": {
                        continueActivity = false;
                    }
                    break;

                    default:
                        break;
                }
            } catch (ClientActionUnhandledException e) {
                System.err.println(e.getMessage());
            }
        }
        return registerAction;
    }
}
