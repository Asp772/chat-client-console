package org.touchsoft.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.touchsoft.exception.ActionToStringTransformException;
import org.touchsoft.model.action.Action;
import org.touchsoft.service.ActionTransformService;

import java.io.IOException;

public class ActionTransformServiceImpl implements ActionTransformService {
    private ObjectMapper objectMapper;

    public ActionTransformServiceImpl() {
        objectMapper = new ObjectMapper();
    }

    @Override
    public String toString(Action action) throws ActionToStringTransformException {
        try {
            return objectMapper.writeValueAsString(action);
        } catch (JsonProcessingException e) {
            throw new ActionToStringTransformException(e.getMessage());
        }
    }

    @Override
    public void displayAction(String action) {
        try {
            JsonNode node = objectMapper.readTree(action);
            System.out.println(node);
//            String type = node.get("type").toString();
//            System.out.println(node.get("type"));
//            System.out.println(node.get("payload"));
//            MessagePayload messagePayload = objectMapper.readValue(productNode.get("payload").toString(),MessagePayload.class);


//            switch (type) {
//                case "LEAVE":
//
//                    break;
//                default:
//                    break;
//            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
