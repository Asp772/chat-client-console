package org.touchsoft.app;

import org.touchsoft.exception.*;
import org.touchsoft.manager.ClientManager;
import org.touchsoft.service.ActionTransformService;
import org.touchsoft.service.ClientActionReceiverService;

public class ChatClient {
    private String serverAddress;
    private ClientManager client;

    public ChatClient(String server_addr, ClientActionReceiverService clientActionReceiverService,

                      ActionTransformService actionTransformService) {
        this.serverAddress = server_addr;
        this.client = new ClientManager(clientActionReceiverService, actionTransformService);
    }

    public void launch() throws ChatClientException {
        try {
            this.client.startClient();
            this.client.connectClient(this.serverAddress);
            this.client.interact();
        } catch (StartClientException e) {
            throw new ChatClientException(e.getMessage());
        } catch (ConnectClientException e) {
            throw new ChatClientException(e.getMessage());
        } catch (WrongServerParameterException e) {
            throw new ChatClientException(e.getMessage());
        } catch (StopClientException e) {
            throw new ChatClientException(e.getMessage());
        }
    }
}
